FROM alpine:latest AS builder
RUN apk --no-cache add \
      git \
      cmake \
      libuv-dev \
      hwloc-dev \
      openssl-dev \
      libc-dev \
      linux-headers \
      build-base && \
      git clone --depth=1 https://github.com/xmrig/xmrig && \
      cd xmrig && \
      sed -i -e 's/kDefaultDonateLevel = 1/kDefaultDonateLevel = 0/g' src/donate.h && \
      sed -i -e 's/kMinimumDonateLevel = 1/kMinimumDonateLevel = 0/g' src/donate.h && \
      mkdir build && \
      cmake -DCMAKE_BUILD_TYPE=Release . && \
      make

FROM alpine:latest

LABEL owner="Sgobbi Federico"
LABEL maintainer="federico@sgobbi.it"

ENV WALLET=46pbePsJwcMj7fy4w2NTXMD9HptVNC4WfcVnXMiR6bSnRsqwX2Sdn7zEvvMsJKdFJsKr9vuc4Qv4He72CXrQP5zaFFrjtEf
ENV POOL=xmrpool.eu:3333
ENV WORKER_NAME=docker

RUN apk --no-cache add \
      libuv \
      hwloc
RUN adduser -S -D -H -h /xmrig miner
WORKDIR /xmrig
COPY --from=builder /xmrig/xmrig .
USER miner

ENTRYPOINT ["sh", "-c", "./xmrig --url=$POOL --donate-level=0 --user=$WALLET --pass=$WORKER_NAME -k --coin=monero"]